
import static java.time.Clock.system;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author akbal
 */
public class LambdaTest {
    public static void main (String[] args){
        FunctionTest ft = () -> System.out.println("Hola Mundo");
        
        //ft.saludar();
        
        LambdaTest objeto = new LambdaTest();
        objeto.mimetodo(ft);
    }
    public void mimetodo(FunctionTest parametro){
        parametro.saludar();
    }
    
}
