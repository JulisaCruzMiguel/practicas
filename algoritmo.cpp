#include <stdio.h>
#include <stdlib.h>
//#include <conio.h>
#include <string.h>
#include <math.h>
int klave1[8][7] = {{57,49,41,33,25,17,9},
                    {1,58,50,42,34,26,18},
                    {10,2,59,51,43,35,27},
                    {19,11,3,60,52,44,36},
                    {63,55,47,39,31,23,15},
                    {7,62,54,46,38,30,22},
                    {14,6,61,53,45,37,29},
                    {21,13,5,28,20,12,4}};
int klave2[8][6] = {{14,17,11,24,1,5},
                    {3,28,15,6,21,10},
                    {23,19,12,4,26,8},
                    {16,7,27,20,13,2},
                    {41,52,31,37,47,55},
                    {30,40,51,45,33,48},
                    {44,49,39,56,34,53},
                    {46,42,50,36,29,32}};
int expandir[8][6] = {{32,1,2,3,4,5},
                      {4,5,6,7,8,9},
                      {8,9,10,11,12,13},
                      {12,13,14,15,16,17},
                      {16,17,18,19,20,21},
                      {20,21,22,23,24,25},
                      {24,25,26,27,28,29},
                      {28,29,30,31,32,1}};
int desplaza[16] = {1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1};
int pmensaje[8][8] = {{58,50,42,34,26,18,10,2},{60,52,44,36,28,20,12,4},{62,54,46,38,30,22,14,6},{64,56,48,40,32,24,16,8},
            {57,49,41,33,25,17,9,1},{59,51,43,35,27,19,11,3},{61,53,45,37,29,21,13,5},{63,55,47,39,31,23,15,7}};
int ptabs[8][4] = {{16, 7, 20, 21},{29, 12, 28, 17},{1, 15, 23, 26},{5, 18, 31, 10},
            {2, 8, 24, 14},{32, 27, 3, 9},{19, 13, 30, 6},{22, 11, 4, 25}};
int cifraf[8][8] = {{40, 8, 48, 16, 56, 24, 64, 32},{39, 7, 47, 15, 55, 23, 63, 31},{38, 6, 46, 14, 54, 22, 62, 30},{37, 5, 45, 13, 53, 21, 61, 29},
         {36, 4, 44, 12, 52, 20, 60, 28},{35, 3, 43, 11, 51, 19, 59, 27},{34, 2, 42, 10, 50, 18, 58, 26},{33, 1, 41, 9, 49, 17, 57, 25}};
int s1[4][16] = {{14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7},{0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8},
     {4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0},{15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}};
int s2[4][16] = {{15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10},{3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5},
     {0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15},{13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}};
int s3[4][16] = {{10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8},{13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1},
     {13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7},{1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}};
int s4[4][16] = {{7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15},{13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9},
     {10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4},{3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}};
int s5[4][16] = {{2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9},{14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6},
     {4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14},{11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}};
int s6[4][16] = {{12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11},{10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8},
     {9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6},{4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}};
int s7[4][16] = {{4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1},{13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6},
     {1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2},{6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}};
int s8[4][16] = {{13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7},{1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2},
      {7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8},{2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}};
char pclave[8][7] = {{'0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0'},
          {'0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0'}};
char pmsg[8][8] = {{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},
        {'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'}};
char fklave[8][6] = {{'0','0','0','0','0','0'},{'0','0','0','0','0','0'},{'0','0','0','0','0','0'},{'0','0','0','0','0','0'},
          {'0','0','0','0','0','0'},{'0','0','0','0','0','0'},{'0','0','0','0','0','0'},{'0','0','0','0','0','0'}};
char pexpan[8][6] = {{'0','0','0','0','0','0'},{'0','0','0','0','0','0'},{'0','0','0','0','0','0'},{'0','0','0','0','0','0'},
          {'0','0','0','0','0','0'},{'0','0','0','0','0','0'},{'0','0','0','0','0','0'},{'0','0','0','0','0','0'}};
char pCajas[8][4] = {{'0','0','0','0'},{'0','0','0','0'},{'0','0','0','0'},{'0','0','0','0'},
          {'0','0','0','0'},{'0','0','0','0'},{'0','0','0','0'},{'0','0','0','0'}};
char msgfin[8][8] = {{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},
        {'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'},{'0','0','0','0','0','0','0','0'}};

char msgB[65]="", keyB[65]="";
char ip[65]="", ko[57]="";
char Rn[17][33]={""}, Ln[17][33]={""};
char Co[29]="", Do[29]="", CnDn[16][57]={""};
char Kn[8][49] = {""};

void mensaje();
void clave(char mensaje[10]);
void cadena(char entrada[8], char salida[65]);
int asciiBinar(int valor);
void trab_clave();
void kdesplazamiento();
void claves_finales();
void cifrado();
void init();
void funcionF(int indice);
int bin_dec(int f, char cad[4]);

int main(){
	mensaje();
	kdesplazamiento();
	claves_finales();
	cifrado();
	return 0;
}
void mensaje(){
	char msg[10];
	printf("\nIntroduce el mensaje de 8 bytes: ");
	scanf("%s",&msg);
	if(strlen(msg)!=8){
		printf("\nMensaje(M) No Valido");
		mensaje();
	}else{
		clave(msg);
	}
}
void clave(char mensaje[10]){
	char key[10];
	printf("\nIntroduce la clave de 8 bytes: ");
	scanf("%s",&key);
	if(strlen(key)!=8){
		printf("\nClave(K) No Valido");
		clave(mensaje);
	}else{
		printf("\nMensaje a Binario");
		cadena(mensaje, msgB);
		printf("\n%s \n\nClave a Binario",msgB);
		cadena(key, keyB);
		printf("\n%s",keyB);

		trab_clave();

	}
}
void cadena(char entrada[8], char salida[64]){
	char aux[8], fin[8];
	int i, as, max=strlen(entrada);

	for(i=0; i<max; i++){
		as = asciiBinar((int)entrada[i]);
		sprintf(aux, "%d", as);

		if(max-strlen(aux)==1){
			strcpy(fin,"0");
		}else{
			strcpy(fin,"00");
		}

		strcat(fin, aux);
		printf("\n%c -> %s",entrada[i], fin);
		strcat(salida, fin);
	}
}

int asciiBinar(int valor){
	int result=0, i=1, r;

	while(valor>0){
		r = valor%2;
		result = result+(i*r);
		valor = valor/2;
		i = i*10;
	}
	return result;
}

void trab_clave(){
	int i, j, index, cont=0;

	printf("\n\nMensaje(M)\n");
	for(i=0; i<8; i++){
		for(j=0; j<8; j++){
			index = pmensaje[i][j];
			pmsg[i][j] = msgB[index-1];
			ip[cont] = pmsg[i][j];
			printf(" %c",ip[cont]);
			if(cont>=32){
				Rn[0][cont-32] = ip[cont];
			}else{
				Ln[0][cont] = ip[cont];
			}
			cont++;
		}
		printf("\n");
	}
	printf("IP(MP) -> %s",ip);
	cont = 0;
	printf("\n\nClave(K)\n");
	for(i=0; i<8; i++){
		for(j=0; j<7; j++){
			index = klave1[i][j];
			pclave[i][j] = keyB[index-1];
			ko[cont] = pclave[i][j];
			printf(" %c",ko[cont]);
			if(cont>=28){
				Do[cont-28] = ko[cont];
			}else{
				Co[cont] = ko[cont];
			}
			cont++;
		}
		printf("\n");
	}
	printf("Ko -> %s\nCo -> %s\nDo -> %s\n",ko,Co,Do);
}

void kdesplazamiento(){
	int i, x;
	char caux[29]="", daux[29] = "";
	char da[29]="", ca[29]="";
	strcpy(caux,Co);
	strcpy(daux,Do);

	for(i=0; i<16; i++){
		if(desplaza[i]==1){
			ca[27] = caux[0];
			da[27] = daux[0];
			for(x=1; x<28; x++){
				ca[x-1] = caux[x];
				da[x-1] = daux[x];
			}
		}else{
			ca[26] = caux[0];
			ca[27] = caux[1];
			da[26] = daux[0];
			da[27] = daux[1];

			for(x=2; x<28; x++){
				ca[x-2] = caux[x];
				da[x-2] = daux[x];
			}
		}
		strcpy(CnDn[i],ca);
		strcat(CnDn[i],da);
		printf("\nC%d -> %s \tD%d - %s",(i+1),ca,(i+1),da);
		strcpy(caux,ca);
		strcpy(daux,da);
	}
}

void claves_finales(){
	int i, j, x, index, cont=0;
	char fin_c[49] = "";

	printf("\n\nClaves Finales (Kn)");
	for(x=0; x<16; x++){
		for(i=0; i<8; i++){
			for(j=0; j<6; j++){
				index = klave2[i][j];
				fklave[i][j] = CnDn[x][index-1];
				fin_c[cont] = fklave[i][j];
				cont++;
			}
		}
		cont = 0;
		strcpy(Kn[x],fin_c);
		printf("\nCD%d -> %s \tK%d -> %s",(x+1),CnDn[x],(x+1),Kn[x]);
	}
}

void init(){
	int i, j;
	for(i=0; i<8; i++){
		for(j=0; j<6; j++){
			pexpan[i][j] = '0';
		}
	}
}

void cifrado(){
	int i, j, index, cont=0;
	char RL[65] = "", cifra[65]="", hexa[5];

	printf("\n\nMensaje(M)\nIP -> %s \n",ip);
	for(i=1; i<17; i++){
		printf("\nL%d -> %s",(i-1),Ln[i-1]);
		printf("\nR%d -> %s\n",(i-1),Rn[i-1]);
		funcionF(i);
		init();
	}
	printf("\nL%d -> %s",(i-1),Ln[i-1]);
	printf("\nR%d -> %s",(i-1),Rn[i-1]);
	strcpy(RL,Rn[i-1]);
	strcat(RL,Ln[i-1]);
	printf("\n\nRL -> %s\n",RL);

	for(i=0; i<8; i++){
		for(j=0; j<8; j++){
			index = cifraf[i][j];
			msgfin[i][j] = RL[index-1];
			cifra[cont] = msgfin[i][j];
			printf(" %c",cifra[cont]);
			cont++;
		}
		printf("\n");
	}
	printf("Ci -> %s\n",cifra);
	cont=0;
	while(cont<64){
		if((cont%8)==0){
			printf(" ");
		}

		hexa[0] = cifra[cont];
		hexa[1] = cifra[cont+1];
		hexa[2] = cifra[cont+2];
		hexa[3] = cifra[cont+3];
		index = bin_dec(4,hexa);
		if(index==15){
			printf("f");
		}
		if(index==14){
			printf("e");
		}
		if(index==13){
			printf("d");
		}
		if(index==12){
			printf("c");
		}
		if(index==11){
			printf("b");
		}
		if(index==10){
			printf("a");
		}
		if(index<10){
			printf("%d",index);
		}
		cont += 4;
	}

	printf("\nMensaje Cifrado");
}

void funcionF(int indice){
	int i, j, index, cont=0, f, c, x;
	char Ern[49]="", Xer[49]="", aux[5]="", Sb[33]="", fi[33]="";

	printf("\n");
	for(i=0; i<8; i++){
		for(j=0; j<6; j++){
			index = expandir[i][j];
			pexpan[i][j] = Rn[indice-1][index-1];
			Ern[cont] = pexpan[i][j];
			printf(" %c",Ern[cont]);
			cont++;
		}
		printf("\n");
	}
	printf("ER%d  -> %s",(indice-1),Ern);
	printf("\nK%d   -> %s",(indice),Kn[indice-1]);

	for(i=0; i<48; i++){
		f = (int)Ern[i];
		c = (int)Kn[indice-1][i];
		x = f^c;
		Xer[i] = x + '0';
	}
	printf("\nExK%d -> %s\n",indice,Xer);
	cont=0;

	while(cont<48){
		aux[0] = Xer[cont];
		aux[1] = Xer[cont+5];
		f = bin_dec(2,aux);

		aux[0] = Xer[cont+1];
		aux[1] = Xer[cont+2];
		aux[2] = Xer[cont+3];
		aux[3] = Xer[cont+4];
		c = bin_dec(4,aux);

		if(cont==0){
			x = s1[f][c];
		}
		if(cont==6){
			x = s2[f][c];
		}
		if(cont==12){
			x = s3[f][c];
		}
		if(cont==18){
			x = s4[f][c];
		}
		if(cont==24){
			x = s5[f][c];
		}
		if(cont==30){
			x = s6[f][c];
		}
		if(cont==36){
			x = s7[f][c];
		}
		if(cont==42){
			x = s8[f][c];
		}
		aux[0] = '0';
		aux[1] = '0';
		aux[2] = '0';
		aux[3] = '0';
		index=3;
		do{
			aux[index] = (x%2) + '0';
			x = x/2;
			index--;
		}while(x!=0);
		strcat(Sb,aux);
		cont += 6;
	}
	printf("\nSB%d -> %s",indice,Sb);
	cont=0;
	for(i=0; i<8; i++){
		for(j=0; j<4; j++){
			index = ptabs[i][j];
			fi[cont] = Sb[index-1];
			cont++;
		}
	}
	printf("\nL%d  -> %s",(indice-1),Ln[indice-1]);
	printf("\nf%d  -> %s",indice,fi);
	for(j=0; j<32; j++){
		f = (int)fi[j];
		c = (int)Ln[indice-1][j];
		x = f^c;
		Sb[j] = x + '0';
	}
	printf("\nR%d  -> %s\n",indice,Sb);
	strcpy(Rn[indice],Sb);
	strcpy(Ln[indice],Rn[indice-1]);
}

int bin_dec(int f, char cad[4]){
	int i, result=0, e=f-1;
	for(i=0; i<f-1; i++){
		if(cad[i]=='1'){
			result += pow(2,e);
		}
		e--;
	}
	if(cad[f-1]=='1'){
		result += 1;
	}
	return result;
}

